<?php

    require_once "classes.php";

    if(isset($_POST['submit'])){
        echo "<div class =\"d-block float-right \"><ol>";
        if(checkForm()===true){
            switch ($_POST['type-switcher']){
                case 'books':
                    $product = new Book($_POST['SKU-input'], $_POST['name-input'], $_POST['price-input'], $_POST['weight-input']);
                    if($product->saveProduct()){
                        echo "Product was added";
                    }
                    break;
                case 'disks':
                    $product = new Disk($_POST['SKU-input'], $_POST['name-input'], $_POST['price-input'], $_POST['size-input']);
                    if($product->saveProduct()) {
                        echo "Product was added";
                    }
                    break;
                case 'furniture':
                    $product = new Furniture($_POST['SKU-input'], $_POST['name-input'], $_POST['price-input'], $_POST['height-input'], $_POST['width-input'], $_POST['length-input']);
                    if($product->saveProduct()) {
                        echo "Product was added";
                    }
                    break;
            }
        }
        echo "</ol></div>";
    };

    function printBook(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "products";
        $conn = new mysqli($servername, $username, $password, $dbname);
        $sql = "SELECT * FROM `books`";
        $result = mysqli_query($conn,$sql);
        if (!$result) {
            echo "error:" . mysqli_error($conn);
            exit;
        }
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                if(!($row['sku']=="")){
                    echo
                    "<div class=\"p-2 m-0 col-xl-2 col-lg-4 col-md-6 col-sm-12 \">
                     <div class=\"p-2 pt-3 w-100 h-100 border\">
                        <ul class=\"list-unstyled text-center\">";
                    echo "<li>" . $row['sku'] . "</li>";
                    echo "<li>" . $row['name'] . "</li>";
                    echo "<li>" . $row['price'] . "$</li>";
                    echo "<li> Weight: " . $row['weight'] . "</li>";
                    echo "</ul>
                    </div>
                </div>";
                }
            }
        }
    }
    function printFurniture(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "products";
        $conn = new mysqli($servername, $username, $password, $dbname);
        $sql = "SELECT * FROM `furniture`";
        $result = mysqli_query($conn,$sql);
        if (!$result) {
            echo "error:" . mysqli_error($conn);
            exit;
        }
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                if(!($row['sku']=="")){
                    echo
                    "<div class=\"p-2 m-0 col-xl-2 col-lg-4 col-md-6 col-sm-12 \">
                         <div class=\"p-2 pt-3 w-100 h-100 border\">
                            <ul class=\"list-unstyled text-center\">";
                    echo "<li>" . $row['sku'] . "</li>";
                    echo "<li>" . $row['name'] . "</li>";
                    echo "<li>" . $row['price'] . "$</li>";
                    echo "<li>Dimension" . $row['height'] . "x" . $row['width'] . "x" . $row['length'] . "</li>";
                    echo "</ul>
                        </div>
                    </div>";
                }
            }
        }
    }

    function printDisks(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "products";
        $conn = new mysqli($servername, $username, $password, $dbname);
        $sql = "SELECT * FROM `disks`";
        $result = mysqli_query($conn,$sql);
        if (!$result) {
            echo "error:" . mysqli_error($conn);
            exit;
        }
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                if(!($row['sku']=="")){
                    echo
                    "<div class=\"p-2 m-0 col-xl-2 col-lg-4 col-md-6 col-sm-12 \">
                         <div class=\"p-2 pt-3 w-100 h-100 border\">
                            <ul class=\"list-unstyled text-center\">";
                    echo "<li>" . $row['sku'] . "</li>";
                    echo "<li>" . $row['name'] . "</li>";
                    echo "<li>" . $row['price'] . " $</li>";
                    echo "<li> Size: " . $row['size'] . " MB</li>";
                    echo "</ul>
                        </div>
                    </div>";
                }
            }
        }
    }

    function checkForm()
    {
       if (isset($_POST['type-switcher'])) {
           $valid_input = true;
           if(empty($_POST['SKU-input'])){
               echo "<li>Fill SKU field</li>";
               $valid_input= false;
           }
           if(empty($_POST['name-input'])){
               echo "<li>Fill name field</li>";
               $valid_input= false;
           }
           if(empty($_POST['price-input'])){
               echo "<li>Fill price field</li>";
               $valid_input= false;
           }
           switch ($_POST['type-switcher']) {
               case 'books':
                   if(empty($_POST['weight-input'])){
                       echo "<li>Fill weight field</li>";
                       $valid_input= false;
                   }

                   break;
               case 'disks':
                   if(empty($_POST['size-input'])){
                       echo "<li>Fill size field</li>";
                       $valid_input = false;
                   }
                   break;
               case 'furniture':
                   if(empty($_POST['length-input'])){
                       echo "<li>Fill length field</li>";
                       $valid_input = false;
                   }
                   if(empty($_POST['width-input'])){
                       echo "<li>Fill width field</li>";
                       $valid_input = false;
                   }
                   if(empty($_POST['height-input'])){
                       echo "<li>Fill height field</li>";
                       $valid_input = false;
                   }
                   break;
           }
           return $valid_input;
       }
       return false;
    }



?>

