<?php ini_set("memory_limit", "16M");?>
<?php
session_start();
include "classes.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <script src="js/jquery-3.4.0.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script>
        $(document).ready(function(){
            $('select').on('change', function()
            {
                $('#book-input').hide();
                $('#furniture-input').hide();
                $('#disk-input').hide();

                if(this.value === "books") {
                    $('#book-input').show();
                }
                if(this.value === "disks"){
                    $('#disk-input').show();
                }
                if(this.value === "furniture"){
                    $('#furniture-input').show();

                }
            });
        });
    </script>
    <title>Product add</title>
</head>
<body>

    <h1 class="h1">Product add</h1>

    <form method="post">
        <div class="form-group" >
            <button type="submit" class="btn btn-primary" name="submit" value="submit">Save</button>
            <hr>
            <label for="SKU-input">SKU</label>
            <input type="text" id="SKU-input" name="SKU-input">
            <br>
            <label for="name-input">Name</label>
            <input type="text" id="name-input" name="name-input">
            <br>
            <label for="price-input">Price</label>
            <input type="number" id="price-input" name="price-input">
            <br>
            <label for="type-switcher">Type switcher</label>
            <select id="type-switcher" name="type-switcher">
            <?php
                $sql = "show tables;";
                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        echo ("<option value = \"". $row["Tables_in_products"] . "\" >");
                        echo (ucfirst($row["Tables_in_products"]));
                        echo ("</option>");
                    }
                }
            ?>

            </select>
            <br>
            <div id="book-input" >
                <label for="weight-input">weight</label>
                <input type="number" id="weight-input" name="weight-input">
                <br>
                <p><i>Input book weight in kg</i></p>
            </div>

            <div id="disk-input" style="display: none">
                <label for="size-input">Size</label>
                <input type="number" id="size-input" name="size-input">
                <br>
                <p><i>Input disk size in MB</i></p>
            </div>
            <div  id="furniture-input" style="display: none">
                <label for="height-input">Height</label>
                <input type="number" id="height-input" name="height-input">
                <br>
                <label for="width-input">Width</label>
                <input type="number" id="width-input" name="width-input">
                <br>
                <label for="length-input">Length</label>
                <input type="number" id="length-input" name="length-input">
                <p><i>Input HxWxL size in cm</i></p>
            </div>
        </div>
    </form>


</body>
</html>