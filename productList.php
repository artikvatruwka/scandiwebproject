<?php ini_set("memory_limit", "16M");?>
<?php
session_start();
require_once "classes.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <script src="js/jquery-3.4.0.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <title>Product list</title>
</head>
<body>

<h1 class="h1">Product list</h1>
<hr>
<div class="row w-75 m-auto">
    <?php
        require_once "classes.php";
        echo printBook();
        echo printDisks();
        echo printFurniture();
    ?>
</div>

</body>
</html>